export const createPhrase = {
    get(id, phrases, placeholder = {}) {
        if (typeof phrases === 'undefined' || typeof phrases[id] === 'undefined') {
            return id;
        }

        const rawPhrase = phrases[id];

        if (!Object.keys(placeholder).length) {
            return rawPhrase;
        }

        const smartCountPhrase = this.smartCount(rawPhrase, placeholder);
        const phrase = this.replacer(smartCountPhrase, placeholder);
        return phrase;
    },

    /*
        split at separator
        choose part depending on count
        replace smart_count
    */
    smartCount(phrase, placeholder) {
        if (typeof placeholder.smart_count  !== 'undefined' && /\|\|\|\|/.exec(phrase)) {
            const pattern = '\\$\\{smart_count\\}';
            const regex = new RegExp(pattern, 'g');
            const count = parseInt(placeholder.smart_count);
            const parts = phrase.split('||||');
            const smartCountPhrase = (count === 1) ? parts[0] : parts[1];
            return smartCountPhrase.replace(regex, count).trim();
        }
        return phrase;
    },

    /*
        replace all placeholders
    */
    replacer(phrase, placeholder) {
        let cleanPhrase = phrase;
        Object.keys(placeholder).forEach(item => {
            const pattern = '\\$\\{' + item + '\\}';
            const regex = new RegExp(pattern, 'g');
            const value = placeholder[item];
            cleanPhrase = cleanPhrase.replace(regex, value);
        });
        return cleanPhrase;
    }
};
